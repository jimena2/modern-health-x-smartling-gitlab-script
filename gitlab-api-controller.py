#! python3
# Modern Health Smartling-GitLab Repository Integration
#
# Downloads completed translations from Smartling and sends them to
# GitLab branches in accordance to custom configurations.
# Created by Donovan Dwyer (ddwyer@smartling.com) for customer Modern Health

import requests
import re
import base64
import json
import os
import shutil
import time
from datetime import datetime
from pathlib import Path

##################################################
#### --- GITLAB REPO MANAGEMENT FUNCTIONS --- ####
##################################################

class config():
    config_data = open('./gitlab-api-config.json').read()
    config_data = re.sub("\/\/ .+", "", config_data)
    config_data = re.sub("/", "\/", config_data)
    config_data = json.loads(config_data)
    gitlab_access_token = config_data['gitlabAccessToken']
    gitlab_api_headers = {'PRIVATE-TOKEN': gitlab_access_token}
    gitlab_base_url = 'https://gitlab.com/api/v4'
    gitlab_repo_url = config_data['gitlabRepoUrl']
    gitlab_repo_id = ''
    source_branch_name = config_data['gitlabSourceBranchName']
    smartling_base_url = 'https://api.smartling.com'
    smartling_user_id = config_data['smartlingUserId']
    smartling_token_secret = config_data['smartlingTokenSecret']
    smartling_project_id = config_data['smartlingProjectId']
    interval_timer = config_data['intervalTimer']

def get_repo_id():
    "Gets the numerical ID for repository using its url"
    repo_url = config.gitlab_repo_url.split('/')
    repo_name = repo_url[-1].split('.')
    seperator = '%2F'
    formatted_path = seperator.join([repo_url[-2], repo_name[0]])
    url = config.gitlab_base_url + '/projects/%s' %(formatted_path)
    r = requests.get(url, headers=config.gitlab_api_headers)
    config.gitlab_repo_id = r.json()['id']

def new_branch(branch_name, repo_id, ref_name):
    "Creates branch using given branch name, repo ID, and reference branch name"
    url = config.gitlab_base_url + '/projects/%s/repository/branches' %(repo_id)
    params = {'branch': branch_name, 'ref': ref_name}
    r = requests.post(url, headers=config.gitlab_api_headers, params=params)
    return r.json()

def delete_branch(repo_id, branch_name):
    "Deletes a given branch"
    url = config.gitlab_base_url + '/projects/%s/repository/branches/%s' %(repo_id, branch_name)
    r = requests.delete(url, headers=config.gitlab_api_headers)

def list_branches(repo_id, **kwargs):
    "Gets list of branches associated with a repo id w/ optional search regex"
    search = kwargs.get('search', None)
    params = {'search': search}
    url = config.gitlab_base_url + '/projects/%s/repository/branches' %(repo_id)
    r = requests.get(url, headers=config.gitlab_api_headers, params=params)
    return r.json()

def create_commit(repo_id, branch_name, locale, file_uri):
    "Commits file to a branch"
    if fetch_from_repo(repo_id, get_abs_file_path_from_uri(file_uri), branch_name) == '404':
        action = 'create'
    else:
        log_capture("File already exists on branch. Commit either updated existing file or did nothing.")
        action = 'update'
    file = open(file_uri).read()
    payload = {
        'branch': branch_name,
        'commit_message': 'Committing %s translated file "%s" to %s branch' %(locale, get_filename_from_uri(file_uri), branch_name),
        'actions': [
            {
                'action': action,
                'file_path': get_abs_file_path_from_uri(file_uri),
                'content': file
            }
        ]
    }
    url = config.gitlab_base_url + '/projects/%s/repository/commits' %(repo_id)
    r = requests.post(url, headers=config.gitlab_api_headers, json=payload)
    return r.json()

def create_merge_request(repo_id, source_branch, target_branch, title):
    "Creates a merge request to specified branch"
    params = {
        'source_branch': source_branch,
        'target_branch': target_branch,
        'title': title
    }
    url = config.gitlab_base_url + '/projects/%s/merge_requests' %(repo_id)
    r = requests.post(url, headers=config.gitlab_api_headers, params=params)
    return r.json()

def fetch_from_repo(repo_id, file_path, branch_name):
    "Downloads file from repository"
    path = re.sub("\/", "%2F", file_path)
    path = re.sub("\.", "%2E", path)
    params = {'ref': branch_name}
    url = config.gitlab_base_url + '/projects/%s/repository/files/%s?' %(repo_id, path)
    r = requests.get(url, headers=config.gitlab_api_headers, params=params)
    parsed_res = r.json()
    if parsed_res == {'message' : '404 File Not Found'}:
        return '404'
    else:
        return base64.decodebytes(parsed_res['content'].encode('ASCII'))

def read_smartling_config(repo_id, branch_name):
    "Returns the Smartling Config JSON file data from a repo branch"
    res = fetch_from_repo(repo_id, 'smartling-config.json', branch_name)
    parsed_res = res.decode('utf-8')
    parsed = re.sub("\/\/[\w\d\s\\\.:\"'()/,+#\-]+\\n", "", parsed_res)
    return json.loads(parsed)

def read_json_file(path):
    "Open JSON file"
    file = open(path).read()
    return json.loads(file)

def write_file(content, file_path):
    "Writes a file when given data and file path"
    with open(file_path, "wb") as file:
        file.write(content)

def compare_branches(repo_id, source_branch, target_branch):
    "Compares two branches and returns the diffs, if any exist"
    params = {
        'from': target_branch,
        'to': source_branch
    }
    url = config.gitlab_base_url + '/projects/%s/repository/compare' %(repo_id)
    r = requests.get(url, headers=config.gitlab_api_headers, params=params)
    parsed_r = r.json()
    return parsed_r['diffs']

################################################
#### --- SMARTLING MANAGEMENT FUNCTIONS --- ####
################################################

def generate_smartling_headers():
    "Generates Smartling access token"
    json_payload = {'userIdentifier': config.smartling_user_id, 'userSecret': config.smartling_token_secret}
    url = config.smartling_base_url + '/auth-api/v2/authenticate'
    r = requests.post(url, json=json_payload)
    parsed_req = r.json()
    if parsed_req['response']['code'] == 'SUCCESS':
        return {'Authorization': 'Bearer %s' %(parsed_req['response']['data']['accessToken'])}

def get_list_of_files(project_id):
    "Fetches list of files uploaded to specified Smartling project"
    url = config.smartling_base_url + '/files-api/v2/projects/%s/files/list' %(project_id)
    r = requests.get(url, headers=generate_smartling_headers())
    parsed_req = r.json()
    if parsed_req['response']['data']['totalCount'] <= 100:
        return parsed_req['response']['data']['items']
    else:
        files = parsed_req['response']['data']['items']
        total = parsed_req['response']['data']['totalCount'] - 100
        offset = 100
        while (total > 0):
            params = {
                'offset': offset,
                'orderBy': 'lastUploaded_desc'
            }
            r = requests.get(url, headers=generate_smartling_headers(), params=params)
            files.extend(r.json()['response']['data']['items'])
            total = total - 100
            offset = offset + 100
        return files

def get_translation_status_for_file(project_id, file_uri):
    "Gets status of translation for each locale in a file"
    params = { 'fileUri': file_uri }
    url = config.smartling_base_url + '/files-api/v2/projects/%s/file/status' %(project_id)
    r = requests.get(url, headers=generate_smartling_headers(), params=params)
    return r.json()

def download_translations(project_id, file_uri, locale, retrieval_type='published', include_org_strs=True):
    "Downloads translated file given specifications and then commits it to repo"
    params = {
        'fileUri': file_uri,
        'retrievalType': retrieval_type,
        'includeOriginalStrings': include_org_strs
    }
    url = config.smartling_base_url + '/files-api/v2/projects/%s/locales/%s/file' %(project_id, locale)
    r = requests.get(url, headers=generate_smartling_headers(), params=params, stream=True)
    file_path = dir_formatter(file_uri, locale)
    log_capture("Dowloading file %s" %(file_uri))
    with open(file_path, 'wb') as translated_file:
        for chunk in r.iter_content(chunk_size=128):
            translated_file.write(chunk)
    log_capture("Committing file %s in %s language to branch %s" %(get_filename_from_uri(file_uri), locale, get_branch_name_from_uri(file_uri)))
    create_commit(config.gitlab_repo_id, get_branch_name_from_uri(file_uri), locale, file_path)

def dir_formatter(file_path, locale, locale_dir=True):
    "Handles the path formatting to preserve dir structure"
    repo = get_repo_name_from_uri(file_path)
    branch = get_branch_name_from_uri(file_path)
    path = get_dir_from_uri(file_path)
    if "en-US" in path:
        path = re.sub("en-US", locale, path)
    else:
        path = "%s/%s" %(locale, path)
    filename = get_filename_from_uri(file_path)
    if locale_dir:
        full_path = "%s/%s/%s/%s" %(repo, branch, path, filename)
        dir_path = "%s/%s/%s" %(repo, branch, path)
    else:
        file_ext = filename.split('.')[-1]
        file = filename.split('.')[0]
        full_path = "%s/%s/%s/%s-%s.%s" %(repo, branch, path, file, locale, file_ext)
        dir_path = "%s/%s/%s/" %(repo, branch, path)
    if os.path.isdir(dir_path):
        shutil.rmtree(dir_path)
    os.makedirs(dir_path)
    return full_path

def get_branch_name_from_uri(file_uri):
    return file_uri.split('/')[2]

def get_repo_name_from_uri(file_uri):
    return file_uri.split('/')[1]

def get_abs_file_path_from_uri(file_uri):
    file_path = file_uri.split('/')
    seperator = '/'
    return seperator.join(file_path[2:])

def get_filename_from_uri(file_uri):
    file_path = file_uri.split('/')
    seperator = '/'
    return seperator.join(file_path[-1:])

def get_dir_from_uri(file_uri):
    file_path = file_uri.split('/')
    seperator = '/'
    return seperator.join(file_path[3:-1])

def log_capture(string, verbose=True):
    "Prints script actions to console and logs them in log file"
    now = datetime.now()
    current_time = now.strftime("%d/%m/%Y %H:%M:%S")
    if verbose:
        print("[%s] - %s" %(current_time, string))

def main():
    while True:
        get_repo_id()
        fileUri_queue = []
        whitelist = []
        blacklist = []
        manifest = {}
        log_capture("Fetching files from Smartling project...")
        request = get_list_of_files(config.smartling_project_id)
        if (len(request) > 0):
            repo_alias = get_repo_name_from_uri(request[0]['fileUri'])
            for file in request:
                if file['fileUri'] not in fileUri_queue:
                    fileUri_queue.append(file['fileUri'])
                    log_capture("Loading file: '%s'" %(file['fileUri']))
                branch_name = get_branch_name_from_uri(file['fileUri'])
                if branch_name not in manifest:
                    locales = []
                    for locale in read_smartling_config(config.gitlab_repo_id, branch_name)['locales']:
                        locales.append(locale['smartling'])
                    log_capture("Loading locales for file '%s'" %(file['fileUri']))
                    manifest[branch_name] = locales
            for branch in manifest.keys():
                if branch not in blacklist:
                    log_capture("Comparing file in branch '%s' against source branch '%s'" %(branch, config.source_branch_name))
                    diffs = compare_branches(config.gitlab_repo_id, branch, config.source_branch_name)
                    if len(diffs) > 1:
                        log_capture(str(len(diffs)) + " diff(s) found")
                        for diff in diffs:
                            if ('/%s/%s/%s' %(repo_alias, branch, diff['new_path'])) in fileUri_queue:
                                log_capture("Adding diff at path %s to queue" %(diff['new_path']))
                                whitelist.append('/%s/%s/%s' %(repo_alias, branch, diff['new_path']))
                    else:
                        log_capture("No diffs found for branch %s" %(branch))
                        blacklist.append(branch)
            for file_uri in whitelist:
                if file_uri in fileUri_queue:
                    log_capture("Checking translation status for %s" %(file_uri))
                    translation_status = get_translation_status_for_file(config.smartling_project_id, file_uri)
                    file_string_count = translation_status['response']['data']['totalStringCount']
                    locale_data = translation_status['response']['data']['items']
                    for locale in locale_data:
                        if locale['completedStringCount'] == file_string_count and locale['localeId'] in manifest[get_branch_name_from_uri(file_uri)]:
                            log_capture("%s translation done for file %s" %(locale['localeId'], file_uri))
                            download_translations(config.smartling_project_id, file_uri, locale['localeId'])
                else:
                    log_capture("File %s removed" %(file_uri))
                    whitelist.remove(file_uri)
        else:
            log_capture("No files exist in Smartling project.")
        time.sleep(config.interval_timer * 60)

if __name__ == "__main__":
    main()
